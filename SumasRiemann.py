#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#import os, sys
import numpy as np
import matplotlib.pyplot as plt

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

# Definir función a integrar
def funcionS(x):
    return (x**3+5*x**2+2*x-8)

# Método para la sumas 
def sumaRiemann(a, b, n):
    h = ((b-a)/n)
    suma=0

    for i in range(n):
        xi = ((a+i)*h)
        suma += funcionS(xi) * h
    return suma


# Limites de integración [a.b]
a=-5
b=2

# Número de rectángulos
n=1000

# Se mandan las variables a la función
resul = sumaRiemann(a, b, n)
print("El resultado de la suma de Riemann es: ", resul)

# Gráficación
delta_h = ((b-a)/n)
x_vals = np.arange(a, b, delta_h)
y_vals = funcionS(x_vals)

plt.plot(x_vals, y_vals, color="green")
plt.bar(x_vals, y_vals, width=((b-a)/n), alpha=0.5, align='edge', facecolor="orange")
plt.xlabel('x')
plt.ylabel('y')
plt.figtext(0.1,0.1,'Sumas de Riemann: ' + str(resul), color="r")
plt.grid(True)
plt.show()

