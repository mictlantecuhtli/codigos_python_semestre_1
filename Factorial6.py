#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

z=1
factorial=int(input("¿Qué número desea ver su factorial?: "))

for i in range (1,factorial, 1):
    resFac=(z*i)
    print( z , " * " , i , " = " , resFac )
    z=(resFac)

print("El factorial de " , factorial , " es: " , z )
