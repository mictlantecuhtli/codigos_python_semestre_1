#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

for i in range ( 0, 8 ,1 ):
        print(' ' * ( ( 8 - i) - 1 ) + "*" * ( (2 * i) + 1 ))

for n in range ( 0 , 4, 1 ):
    print("     * * *")
