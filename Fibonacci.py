#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

x = 0
y = 1
z = 1

numero = int(input("Digite número para serie de Fibonacci: "))

for i in range ( 1, numero, 1 ):
    c = 1
    for a in range( 1, (( numero - i) + 1), 1 ):
        print(" ", end="")
    print(" ")
    for b in range ( 1, i, 1 ):
        print(c , " ", end="")
        c = ( c * ( i - b ) / b )
    print("")
