#!/usr/local/bin/pythonc
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

descuento=0
totalPagar=0
totalPagarDescuento=0

print("¿De qué color es la petola que sacaste?: ")
print("1.-Verde")
print("2.-Amarilla")
print("3.-Roja")
color=int(input("Digite su opción: "))

if color == 1:
    print("Felicidades, saco la pelota verde, tiene un 10% de descuento")
    totalPagar=int(input("¿Cuál es su monto a pagar?: "))
    descuento = ( totalPagar * 0.10)
    totalPagarDescuento = ( totalPagar - descuento )
elif color == 2:
    print("Felicidades, saco la pelota amarilla, tiene un 5% de descuento")
    totalPagar=int(input("¿Cuál es su monto a pagar?: "))
    descuento = ( totalPagar * 0.05)
    totalPagarDescuento = ( totalPagar - descuento )
elif color == 3:
    print("Felicidades, saco la pelota roja, tiene un 15% de descuento")
    totalPagar=int(input("¿Cuál es su monto a pagar?: "))
    descuento = ( totalPagar * 0.15)
    totalPagarDescuento = ( totalPagar - descuento )
else:
    print("Opción inválida")

print("El total a pagar es: " , totalPagarDescuento , " pesos")
