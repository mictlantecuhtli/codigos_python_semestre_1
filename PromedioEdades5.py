#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

cantidad=3
sumaEdad=0
promedioEdades=0

for i in range ( 0, cantidad, 1):
    edad=int(input("Ingrese la edad del alumno: "))
    sumaEdad = ( sumaEdad + edad )


promedioEdades = ( sumaEdad / 3 )
print("El promedio de edades es: " , promedioEdades)

