#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

cantidad=0
total=0

print("Selecciona la talla")
print("1.-Talla chica")
print("2.-Talla mediana")
print("3.-Talla grande")
opcion=int(input("Digite su opción: "))

if opcion == 1:
    print("Haz elegido la talla chica (Talla 7)")
    cantidad=int(input("¿Cuántos vestidos haz comprado?: "))
    total = ( cantidad * 300 )
if opcion == 2:
    print("Haz elegido la talla mediana (Talla 12)")
    cantidad=int(input("¿Cuántos vestidos haz comprado?: "))
    total = ( cantidad * 400 )
if opcion == 3:
    print("Haz elegido la talla grande (Talla 16)")
    cantidad=int(input("¿Cuántos vestidos haz comprado?: "))
    total = ( cantidad * 500 )


print("El total a pagar es de: " , total , " pesos" )

