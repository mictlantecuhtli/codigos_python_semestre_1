#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#import os, sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

def funcionPlot(fx, a, b, ra, rb, n):
    h = ((b-a)/n)
    x = np.arange(a, b+h, h)
    plt.plot(x,fx(x), color="green")

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("Área bajo la curva. Sumas de Riemann para fx(x)")
    plt.figtext(0.1,00.2,"El área bajo la curva es: " + str(riemann_sum), color="r")
    plt.show()

def fx(x):
    return x**2

funcionPlot(fx,0,1,1.1,1,100)
