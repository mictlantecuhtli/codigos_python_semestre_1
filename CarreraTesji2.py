#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

cred=0

print("¿Participaste en el desfile?")
print("1.-Si")
print("0.-No")
opcion=int(input("Digite su opción: "))

if opcion == 1:
    cred = ( cred + 1 )
    print("Ganaste un crédito, ¿En qué taller estas inscrito?: ")
    print("1.-Danza")
    print("2.-Basquetbol")
    print("3.-Futbol")
    print("4.-TKD")
    print("5.-Voleibol")
    taller=int(input("Digite su opción: "))

    if taller == 1:
        print("Uniforme escolar")
    elif taller == 2:
        print("Blanco")
    elif taller == 3:
        print("Verde")
    elif taller == 4:
        print("Uniforme TKD")
    elif taller == 5:
        print("Rojo")
    else:
        print("Opción inválida")
else:
    print("No tienes ningún crédito")

print("¿Participaste en la carrera?")
print("1.-Si")
print("0.-No")
carrera=int(input("Digite su opcíon: "))

if carrera == 1:
    print("Ganaste otro crédito")
    cred = ( cred + 1 )
    print("¿Quedaste en uno de los 3 primeros lugares?: ")
    print("1.-Si")
    print("0.-No")
    lugar=int(input("Digite su opción: "))

    if lugar == 1:
        print("Ganaste otro crédito")
        cred = ( cred + 1 )
    else:
        cred = ( cred + 0 )
    
else:
    cred = ( cred + 0 )


print("El total de créditos obtenidos es: " , cred )

