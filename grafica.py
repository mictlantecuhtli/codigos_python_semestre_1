import numpy as np
import matplotlib.pyplot as plt

# Definir la función a integrar
def funcion_a_integrar(x):
    return x**2  # Puedes cambiar esta función a cualquier otra que desees integrar

# Método del punto medio para sumas de Riemann
def suma_riemann_punto_medio(func, a, b, n):
    dx = (b - a) / n
    total = 0
    for i in range(n):
        xi = a + (i + 0.5) * dx
        total += func(xi)
    return total * dx

# Intervalo de integración [a, b]
a = 0
b = 2

# Número de subintervalos (a mayor n, mayor precisión)
n = 100

# Calcular la suma de Riemann usando el método del punto medio
resultado_riemann = suma_riemann_punto_medio(funcion_a_integrar, a, b, n)
print("Resultado de la suma de Riemann:", resultado_riemann)

# Graficar la función y las áreas aproximadas por las sumas de Riemann
x_vals = np.linspace(a, b, 1000)
y_vals = funcion_a_integrar(x_vals)

fig, ax = plt.subplots()
ax.plot(x_vals, y_vals, label='Función a integrar: $x^2$')
ax.bar(x_vals, y_vals, width=(b-a)/n, alpha=0.2, align='edge', label='Áreas aproximadas por sumas de Riemann')
ax.legend()
plt.xlabel('x')
plt.ylabel('y')
plt.title('Sumas de Riemann')
plt.grid(True)
plt.show()

