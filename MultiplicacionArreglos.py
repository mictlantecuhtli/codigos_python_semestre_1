#!/usr/local/bin/pythonc
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

a1 = []
a2 = []
result = []
val1=0
val2=0
b=4

print("")
print("Arreglo 1")

for i in range ( 0 , 5 , 1 ):
    print("Ingresa el número en la posición " , i , ": ")
    val1=int(input())
    a1.append(val1)

print("")
print("Arreglo 2")

for y in range ( 0 , 5 , 1 ):
    print("Ingrese el número en la posición " , y , ": ")
    val2=int(input())
    a2.append(val2)

print("")
print("Resultado")
print("")

for a in range ( 0 , 5 , 1 ):
    mul = ( a1[a] * a2[b] ) 
    result.append(mul)
    result[a] = (mul)
    b = ( b - 1 )

for c in range ( 0 , 5 , 1 ):
    print(a1[c] , " " , end="")

print("")
print("")

for d in range ( 0 , 5, 1 ):
    print(a2[d] , " " , end="")


print("")
print("")

for e in range ( 0 , 5 , 1 ):
    print(result[e] , " " , end="")

print("")
