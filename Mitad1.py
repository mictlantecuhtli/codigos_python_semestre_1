#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 29-01-2023                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

numero=int(input("Digite un número del (0-200): "))

if numero > 0 and numero <= 200:
    doble = ( numero * 2 )
    mitad = ( doble / 2 )
    tercera = ( mitad / 3 )
    print ("La tercera parte de la mitad del doble de un número es: " , tercera)
else:
    print ("Número inválido")
